package rockPaperScissors;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.Random;

public class RockPaperScissors {
	
	public static void main(String[] args) {
    	/* 	
    	 * The code here does two things:
    	 * It first creates a new RockPaperScissors -object with the
    	 * code `new RockPaperScissors()`. Then it calls the `run()`
    	 * method on the newly created object.
         */
        new RockPaperScissors().run();
    }
    
    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    String [] rpsChoices = {"rock", "paper", "scissors"};
    Boolean continuePlaying = true;
    
    public void run() {
    
        while (true) {
            Random random = new Random();
            int randomNumber = random.nextInt(rpsChoices.length);
            String computerMove = rpsChoices[randomNumber];
            String playerMove;

            
            while (true) {
                System.out.println("Let's play round " + roundCounter + "!");
                System.out.println("Your choice (Rock/Paper/Scissors)?");

                playerMove = sc.nextLine();
                if (playerMove.equals("rock") || playerMove.equals("paper") ||  playerMove.equals("scissors")) {
                    break;
                }
                System.out.println("I do not understand " + playerMove + ". Could you try again?");
            }
            if (playerMove.equals(computerMove)) {
                System.out.println("Human chose " + playerMove + ", computer chose " + computerMove + ". It's a tie!");
                roundCounter++;
            }
            else if (playerMove.equals("rock")) {
                if (computerMove.equals("paper")) {
                    System.out.println("Human chose " + playerMove + ", computer chose " + computerMove + ". Computer wins!");
                    roundCounter++;
                    computerScore++;
                } else if (computerMove.equals("scissors")) {
                    System.out.println("Human chose " + playerMove + ", computer chose " + computerMove + ". Human wins");
                    roundCounter++;
                    humanScore++;
                }
            }
            else if (playerMove.equals("paper")) {
                if (computerMove.equals("rock")) {
                    System.out.println("Human chose " + playerMove + ", computer chose " + computerMove + ". Human wins!");
                    roundCounter++;
                    humanScore++;
                } else if (computerMove.equals("scissors")) {
                    System.out.println("Human chose " + playerMove + ", computer chose " + computerMove + ". Computer wins");
                    roundCounter++;
                    computerScore++;
                }
            }
            else if (playerMove.equals("scissors")) {
                if (computerMove.equals("rock")) {
                    System.out.println("Human chose " + playerMove + ", computer chose " + computerMove + ". Computer wins!");
                    roundCounter++;
                    computerScore++;
                    
                } else if (computerMove.equals("paper")) {
                    System.out.println("Human chose " + playerMove + ", computer chose " + computerMove + ". Human wins");
                    roundCounter++;
                    humanScore++;
                }
            }
            System.out.println("Score: human " + humanScore + ", computer " + computerScore);
            System.out.println("Do you wish to continue playing? (y/n)?");
            String playAgain = sc.next();
            if (!playAgain.equals("y")) {
                System.out.println("Bye Bye!");
                break;
            }
        }
    }
    /**
        Jeg får ikke til å avslutte programmet med siste break funksjon på linje 89. Skjønner ikke helt hva jeg overser her for å være helt ærlig.
        Ellers funker programmet som det skal.
    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }

}
